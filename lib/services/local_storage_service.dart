import 'package:localstorage/localstorage.dart';

class LocalStorageService {
  static final LocalStorageService _localStorageService = LocalStorageService._internal();

  factory LocalStorageService() {
    return _localStorageService;
  }

  LocalStorageService._internal();

  Future<void> addNewItem(String key, String value) async {
    final LocalStorage storage = LocalStorage('k3y_logger_storage');
    await storage.ready;
    await storage.setItem(key, value);
  }

  Future<dynamic> getItem(String key) async {
    final LocalStorage storage = LocalStorage('k3y_logger_storage');
    await storage.ready;
    return await storage.getItem(key);
  }
}
