import 'package:intl/intl.dart';

String getFormattedDateLong(DateTime date) {
  return DateFormat('yyyy-MM-dd').format(date);
}
