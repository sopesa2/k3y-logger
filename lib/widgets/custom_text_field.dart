import 'package:flutter/material.dart';

class CustomTextField extends StatelessWidget {
  final Function(String)? onChanged;
  final String? hintText;
  final Icon? postfix;
  CustomTextField(
      {super.key, required this.postfix, this.hintText, this.onChanged});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.fromLTRB(5, 0, 5, 0),
      padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 5),
      decoration: BoxDecoration(
        border: Border.all(
          color: const Color.fromARGB(248, 120, 124, 132),
        ),
        borderRadius: BorderRadius.circular(20),
        shape: BoxShape.rectangle,
      ),
      child: TextFormField(
        onChanged: onChanged,
        decoration: InputDecoration(
          suffixIcon: Padding(
            padding: EdgeInsets.all(2),
            child: postfix,
          ),
          hintText: hintText,
        ),
      ),
    );
  }
}
