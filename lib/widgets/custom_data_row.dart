import 'package:flutter/widgets.dart';

class CustomDataRow extends StatelessWidget {
  final String title;
  final String? data;
  final int flex1;
  final int flex2;

  const CustomDataRow({
    super.key,
    required this.title,
    required this.data,
    this.flex1 = 3,
    this.flex2 = 5,
  });

  @override
  Widget build(BuildContext context) {
    if (data == null) {
      return Container();
    }

    return Row(
      children: [
        Expanded(
          flex: flex1,
          child: Text(
            title,
            style: TextStyle(fontSize: 15, fontWeight: FontWeight.w500),
          ),
        ),
        Expanded(
          flex: flex2,
          child: Text(
            data!,
            style: const TextStyle(fontSize: 15, fontWeight: FontWeight.w300),
          ),
        ),
      ],
    );
  }
}
