import 'package:flutter/material.dart';

class CustomSearchField extends StatelessWidget {
  final Function(String) searchFunction;
  final String? hintText;
  final String? initialValue;

  CustomSearchField({super.key, required this.searchFunction, this.hintText, required this.initialValue});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.fromLTRB(5, 0, 5, 0),
      padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 5),
      decoration: BoxDecoration(
        border: Border.all(
          color: const Color.fromARGB(248, 120, 124, 132),
        ),
        borderRadius: BorderRadius.circular(20),
        shape: BoxShape.rectangle,
      ),
      child: TextFormField(
        initialValue: initialValue,
        onChanged: searchFunction,
        // Későbbi név/kulcs listára autofill lehetőség. - DobDan
        //autofillHints: ,
        textCapitalization: TextCapitalization.words,
        decoration: InputDecoration(
          prefixIcon: const Padding(
            padding: EdgeInsets.all(2),
            child: Icon(Icons.search),
          ),
          hintText: hintText,
        ),
      ),
    );
  }
}
