import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:k3y_logger/bloc/keys/keys_bloc.dart';
import 'package:k3y_logger/bloc/logs/logs_bloc.dart';
import 'package:k3y_logger/bloc/logs/logs_event.dart';
import 'package:k3y_logger/bloc/people/people_bloc.dart';
import 'package:k3y_logger/core/date_format.dart';
import 'package:k3y_logger/model/log.dart';
import 'package:k3y_logger/model/logged_key.dart';
import 'package:k3y_logger/model/person.dart';
import 'package:k3y_logger/widgets/custom_button.dart';

class CreateLogDialog extends StatefulWidget {
  @override
  State<CreateLogDialog> createState() {
    return _CreateLogDialogState();
  }
}

class _CreateLogDialogState extends State<CreateLogDialog> {
  Person? selectedPerson;
  LoggedKey? selectedKey;
  DateTime? from;
  DateTime? to;

  @override
  Widget build(BuildContext context) {
    return Dialog(
      insetPadding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
      child: Padding(
        padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                Expanded(
                  flex: 1,
                  child: Text(
                    'Person:',
                    style: TextStyle(
                      color: Colors.teal,
                      fontSize: 20,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                ),
                Expanded(
                  flex: 3,
                  child: DropdownButton<Person>(
                    isExpanded: true,
                    value: selectedPerson,
                    icon: const Icon(Icons.keyboard_arrow_down),
                    elevation: 16,
                    style: TextStyle(color: Colors.teal.shade900),
                    underline: Container(
                      height: 2,
                      color: Colors.teal,
                    ),
                    onChanged: (Person? value) {
                      setState(() {
                        selectedPerson = value;
                      });
                    },
                    items: BlocProvider.of<PeopleBloc>(context)
                        .state
                        .allPeople!
                        .map<DropdownMenuItem<Person>>((Person value) {
                      return DropdownMenuItem<Person>(
                        value: value,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text('${value.name}'),
                            Text('${value.email}'),
                          ],
                        ),
                      );
                    }).toList(),
                  ),
                ),
              ],
            ),
            Row(
              children: [
                Expanded(
                  flex: 1,
                  child: Text(
                    'Key:',
                    style: TextStyle(
                      color: Colors.teal,
                      fontSize: 20,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                ),
                Expanded(
                  flex: 3,
                  child: DropdownButton<LoggedKey>(
                    isExpanded: true,
                    value: selectedKey,
                    icon: const Icon(Icons.keyboard_arrow_down),
                    elevation: 16,
                    style: TextStyle(color: Colors.teal.shade900),
                    underline: Container(
                      height: 2,
                      color: Colors.teal,
                    ),
                    onChanged: (LoggedKey? value) {
                      setState(() {
                        selectedKey = value;
                      });
                    },
                    items: BlocProvider.of<KeysBloc>(context)
                        .state
                        .allKeys!
                        .map<DropdownMenuItem<LoggedKey>>((LoggedKey value) {
                      return DropdownMenuItem<LoggedKey>(
                        value: value,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text('${value.name}'),
                            Text('${value.location}'),
                          ],
                        ),
                      );
                    }).toList(),
                  ),
                ),
              ],
            ),
            CustomButton(
              action: () async {
                from = await showDatePicker(
                  context: context,
                  initialDate: DateTime.now(),
                  firstDate: DateTime(2020),
                  lastDate: DateTime(2100),
                );
                setState(() {});
              },
              label: (from == null) ? 'From' : getFormattedDateLong(from!),
              importanceStyle: Importance.Secondary,
              icon: Icon(Icons.calendar_month),
            ),
            CustomButton(
              action: () async {
                to = await showDatePicker(
                  context: context,
                  initialDate: DateTime.now(),
                  firstDate: DateTime(2020),
                  lastDate: DateTime(2100),
                );
                setState(() {});
              },
              label: (to == null) ? 'To' : getFormattedDateLong(to!),
              importanceStyle: Importance.Secondary,
              icon: Icon(Icons.calendar_month),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                CustomButton(
                  active: selectedKey != null &&
                      selectedPerson != null &&
                      from != null &&
                      to != null,
                  action: () {
                    BlocProvider.of<LogsBloc>(context).add(
                      LogsEventAddNewLog(
                        newLog: Log(
                          key: selectedKey!,
                          person: selectedPerson!,
                          startDate: from!,
                          endDate: to!,
                        ),
                      ),
                    );
                    Navigator.pop(context);
                  },
                  label: 'Add log',
                  importanceStyle: Importance.Primary,
                ),
                CustomButton(
                  action: () => Navigator.pop(context),
                  label: 'Cancel',
                  importanceStyle: Importance.Secondary,
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
