import 'package:flutter/material.dart';

class CustomExpandableContent extends StatefulWidget {
  final String title;
  final Widget content;

  const CustomExpandableContent({
    required this.title,
    required this.content,
    super.key,
  });

  @override
  State<CustomExpandableContent> createState() => _CustomExpandableContentState();
}

class _CustomExpandableContentState extends State<CustomExpandableContent> with SingleTickerProviderStateMixin {
  bool _isExpanded = false;

  late AnimationController _controller;
  late Animation<double> curve;

  @override
  void initState() {
    _controller = AnimationController(duration: const Duration(milliseconds: 250), vsync: this);
    curve = CurvedAnimation(
      parent: _controller,
      curve: Curves.easeIn,
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.all(
          Radius.circular(18),
        ),
      ),
      margin: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
      color: Colors.teal.shade100,
      elevation: 0,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Padding(
            padding: EdgeInsets.only(left: 15, right: 20),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Expanded(
                  flex: 9,
                  child: SingleChildScrollView(
                    scrollDirection: Axis.horizontal,
                    child: Text(
                      widget.title,
                      style: TextStyle(
                        fontSize: 22,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                  ),
                ),
                IconButton(
                  constraints: BoxConstraints(),
                  padding: EdgeInsets.symmetric(vertical: 10),
                  icon: Icon(
                    (_isExpanded) ? Icons.keyboard_arrow_up : Icons.keyboard_arrow_down,
                  ),
                  onPressed: () {
                    setState(() => _isExpanded = !_isExpanded);
                    (_isExpanded) ? _controller.animateTo(1) : _controller.animateBack(0);
                  },
                ),
              ],
            ),
          ),
          AnimatedSize(
            curve: Curves.fastOutSlowIn,
            duration: Duration(milliseconds: 150),
            child: Container(
              padding: (_isExpanded) ? EdgeInsets.only(bottom: 10) : null,
              child: !_isExpanded
                  ? null
                  : FadeTransition(
                      opacity: curve,
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Divider(
                            height: 1,
                            color: Colors.teal.shade800,
                            thickness: 2,
                            indent: 0,
                            endIndent: 20,
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          widget.content,
                        ],
                      ),
                    ),
            ),
          ),
        ],
      ),
    );
  }
}
