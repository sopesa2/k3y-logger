import 'package:flutter/material.dart';

class CustomTabItem extends StatelessWidget {
  final Function() onTapFunction;
  final IconData icon;
  final bool active;

  CustomTabItem({
    super.key,
    required this.icon,
    required this.onTapFunction,
    required this.active,
  });

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Padding(
        padding: const EdgeInsets.all(1.0),
        child: ElevatedButton(
          onPressed: onTapFunction,
          child: Icon(
            icon,
            color: (active) ? Colors.teal.shade50 : Colors.teal.shade900,
          ),
          style: ElevatedButton.styleFrom(
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
            padding: EdgeInsets.all(2),
          ),
        ),
      ),
    );
  }
}
