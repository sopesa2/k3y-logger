import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:k3y_logger/core/show_snackbar.dart';
import 'package:k3y_logger/model/logged_key.dart';
import 'package:k3y_logger/widgets/custom_button.dart';

import 'package:path_provider/path_provider.dart';
import 'package:pdf/pdf.dart';
import 'package:pdf/widgets.dart' as pw;
import 'package:permission_handler/permission_handler.dart';

class GenerateQrCodeButton extends StatelessWidget {
  final LoggedKey loggedKey;

  const GenerateQrCodeButton({super.key, required this.loggedKey});

  @override
  Widget build(BuildContext context) {
    return CustomButton(
      action: () async {
        try {
          bool permission = await _askPermission();
          if (permission) {
            final pdf = pw.Document();
            pdf.addPage(
              pw.Page(
                pageFormat: PdfPageFormat.a4,
                build: (pw.Context context) {
                  return pw.BarcodeWidget(
                    // height: 70,
                    // width: 70,
                    color: PdfColor.fromHex("#000000"),
                    barcode: pw.Barcode.qrCode(),
                    data: 'k3ylog:${jsonEncode(loggedKey.toJson())}',
                  );
                },
              ),
            ); // Page
            Directory? directory;
            if (Platform.isIOS) {
              directory = await getApplicationDocumentsDirectory();
            } else {
              directory = Directory('/storage/emulated/0/Download');
              // Put file in global download folder, if for an unknown reason it didn't exist, we fallback
              // ignore: avoid_slow_async_io
              if (!await directory.exists()) directory = await getExternalStorageDirectory();
            }
            final file = File("${directory!.path}/${loggedKey.name}_qr_code.pdf");
            await file.writeAsBytes(await pdf.save());
            showSnackbar(message: 'QR code saved to downloads folder!', color: Colors.green, context: context);
          } else {
            showSnackbar(message: 'Permission denied', color: Colors.red, context: context);
          }
        } catch (e) {
          showSnackbar(message: 'Error', color: Colors.red, context: context);
        }
      },
      label: 'Get QR code',
      importanceStyle: Importance.Primary,
      icon: Icon(Icons.qr_code_2),
    );
  }

  Future<bool> _askPermission() async {
    if (await Permission.storage.isDenied) {
      PermissionStatus result;
      if (Platform.isAndroid) {
        result = await Permission.storage.request();
      } else {
        result = await Permission.photos.request();
      }

      return result.isGranted;
    } else {
      return true;
    }
  }
}
