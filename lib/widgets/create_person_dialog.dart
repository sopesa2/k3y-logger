import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:k3y_logger/bloc/people/people_bloc.dart';
import 'package:k3y_logger/bloc/people/people_event.dart';
import 'package:k3y_logger/core/date_format.dart';
import 'package:k3y_logger/model/person.dart';
import 'package:k3y_logger/widgets/custom_button.dart';
import 'package:k3y_logger/widgets/custom_text_field.dart';

class CreatePersonDialog extends StatefulWidget {
  @override
  State<CreatePersonDialog> createState() {
    return _CreatePersonDialogState();
  }
}

class _CreatePersonDialogState extends State<CreatePersonDialog> {
  String? inputName;
  String? inputEmail;
  String? inputPhone;
  DateTime? dateOfBirth;

  @override
  Widget build(BuildContext context) {
    return Dialog(
      insetPadding: EdgeInsets.symmetric(horizontal: 5, vertical: 5),
      child: Padding(
        padding: EdgeInsets.symmetric(vertical: 8, horizontal: 8),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                Expanded(
                  flex: 1,
                  child: Text(
                    'Name:',
                    style: TextStyle(
                      color: Colors.teal,
                      fontSize: 20,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                ),
                Expanded(
                  flex: 3,
                  child: CustomTextField(
                    postfix: Icon(Icons.account_circle_outlined),
                    hintText: "Type name here...",
                    onChanged: (val) {
                      inputName = val;
                    },
                  ),
                ),
              ],
            ),
            Row(
              children: [
                Expanded(
                  flex: 1,
                  child: Text(
                    'Email:',
                    style: TextStyle(
                      color: Colors.teal,
                      fontSize: 20,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                ),
                Expanded(
                  flex: 3,
                  child: CustomTextField(
                    postfix: Icon(Icons.email),
                    hintText: "Type email here...",
                    onChanged: (val) {
                      inputEmail = val;
                    },
                  ),
                ),
              ],
            ),
            Row(
              children: [
                Expanded(
                  flex: 1,
                  child: Text(
                    'Phone:',
                    style: TextStyle(
                      color: Colors.teal,
                      fontSize: 20,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                ),
                Expanded(
                  flex: 3,
                  child: CustomTextField(
                    postfix: Icon(Icons.phone),
                    hintText: "Type phone number here...",
                    onChanged: (val) {
                      inputPhone = val;
                    },
                  ),
                ),
              ],
            ),
            CustomButton(
              action: () async {
                dateOfBirth = await showDatePicker(
                  context: context,
                  initialDate: DateTime.now(),
                  firstDate: DateTime(1920),
                  lastDate: DateTime(2023),
                );
                setState(() {});
              },
              label: (dateOfBirth == null) ? 'Date of birth' : getFormattedDateLong(dateOfBirth!),
              importanceStyle: Importance.Secondary,
              icon: Icon(Icons.calendar_month),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Expanded(
                  flex: 5,
                  child: CustomButton(
                    active: inputName != "" && inputName != null && inputEmail != "" && inputEmail != null && inputPhone != "" && inputPhone != null && dateOfBirth != null,
                    action: () {
                      BlocProvider.of<PeopleBloc>(context).add(
                        PeopleEventAddNewPerson(
                          newPerson: Person(
                            name: inputName!,
                            email: inputEmail!,
                            phoneNumber: inputPhone!,
                            dateOfBirth: dateOfBirth!,
                          ),
                        ),
                      );
                      Navigator.pop(context);
                    },
                    label: 'Add person',
                    importanceStyle: Importance.Primary,
                  ),
                ),
                SizedBox(
                  width: 3.5,
                ),
                Expanded(
                  flex: 4,
                  child: CustomButton(
                    action: () => Navigator.pop(context),
                    label: 'Cancel',
                    importanceStyle: Importance.Secondary,
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
