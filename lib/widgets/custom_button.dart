import 'package:flutter/material.dart';

enum Importance { Primary, Secondary, Warning }

class CustomButton extends StatelessWidget {
  const CustomButton({required this.action, required this.label, required this.importanceStyle, super.key, this.icon, this.active = true});
  final void Function() action;
  final String label;
  final Importance importanceStyle;
  final Icon? icon;
  final bool active;

  @override
  Widget build(BuildContext context) {
    final ButtonStyle style;
    if (importanceStyle == Importance.Warning) {
      style = ElevatedButton.styleFrom(
        backgroundColor: const Color.fromARGB(255, 255, 3, 3),
        textStyle: const TextStyle(
          fontSize: 20,
          fontWeight: FontWeight.w500,
          color: Color.fromARGB(255, 255, 255, 255),
        ),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(15)),
          side: BorderSide(
            color: const Color.fromARGB(255, 255, 3, 3),
          ),
        ),
      );
    } else if (importanceStyle == Importance.Primary) {
      style = ElevatedButton.styleFrom(
        backgroundColor: Colors.teal.shade600,
        textStyle: const TextStyle(
          fontSize: 20,
          fontWeight: FontWeight.w500,
          color: Color.fromARGB(255, 255, 255, 255),
        ),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(15)),
          side: BorderSide(
            color: Colors.teal.shade600,
          ),
        ),
      );
    } else {
      style = ElevatedButton.styleFrom(
        shadowColor: Colors.transparent,
        foregroundColor: Colors.teal.shade600,
        backgroundColor: Colors.transparent,
        textStyle: const TextStyle(
          fontSize: 20,
          fontWeight: FontWeight.w500,
        ),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(15)),
          side: BorderSide(
            width: 1,
            color: Colors.teal.shade600,
          ),
        ),
      );
    }

    return ElevatedButton(
      style: style,
      onPressed: (active) ? action : null,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 5),
        child: (icon == null)
            ? Text(label)
            : Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(label),
                  icon!,
                ],
              ),
      ),
    );
  }
}
