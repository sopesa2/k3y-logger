import 'package:flutter/material.dart';

class CustomAddButton extends StatelessWidget {
  const CustomAddButton(this.btnFunc, {super.key});
  final void Function() btnFunc;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: btnFunc,
      child: Container(
        decoration: BoxDecoration(
            color: Colors.teal.shade600,
            borderRadius: BorderRadius.circular(12)),
        padding: EdgeInsets.all(10),
        child: Icon(
          Icons.add_sharp,
          size: 30.0,
          color: Colors.teal.shade100,
        ),
      ),
    );
  }
}
