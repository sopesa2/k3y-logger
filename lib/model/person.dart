import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

part 'person.g.dart';

@JsonSerializable()
class Person extends Equatable {
  final String name;
  final String email;
  final String phoneNumber;
  final DateTime dateOfBirth;
  // final File photo; TODO -> initially it's not necessary

  const Person({
    required this.name,
    required this.email,
    required this.phoneNumber,
    required this.dateOfBirth,
  });

  factory Person.fromJson(Map<String, dynamic> json) => _$PersonFromJson(json);
  Map<String, dynamic> toJson() => _$PersonToJson(this);

  @override
  List<Object?> get props => [name, email, phoneNumber, dateOfBirth];
}
