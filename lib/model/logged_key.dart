import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

part 'logged_key.g.dart';

enum KeyType { key, magnetCard, nfcTag }

extension KeyTypeExtension on KeyType {
  String get typeName {
    switch (this) {
      case KeyType.key:
        return "Key";
      case KeyType.magnetCard:
        return "Magnet Card";
      case KeyType.nfcTag:
        return "Nfc Tag";
    }
  }
}

enum AuthLevel { owner, security, staff, anyone }

extension AuthLevelExtension on AuthLevel {
  String get authLevelName {
    switch (this) {
      case AuthLevel.owner:
        return "Owner";
      case AuthLevel.security:
        return "Security";
      case AuthLevel.staff:
        return "Staff";
      case AuthLevel.anyone:
        return "Public";
    }
  }
}

@JsonSerializable()
class LoggedKey extends Equatable {
  final String name;
  final String location;
  final AuthLevel authenticationLevel;
  final DateTime logTime;
  final KeyType? type;
  final String? group;

  const LoggedKey({
    required this.name,
    required this.location,
    required this.logTime,
    required this.authenticationLevel,
    this.type,
    this.group,
  });

  factory LoggedKey.fromJson(Map<String, dynamic> json) => _$LoggedKeyFromJson(json);
  Map<String, dynamic> toJson() => _$LoggedKeyToJson(this);

  @override
  List<Object?> get props => [name, location, type, group, authenticationLevel, logTime];
}
