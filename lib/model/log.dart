import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:k3y_logger/model/logged_key.dart';
import 'package:k3y_logger/model/person.dart';

part 'log.g.dart';

@JsonSerializable()
class Log extends Equatable {
  final LoggedKey key;
  final Person person;
  final DateTime startDate;
  final DateTime endDate;

  const Log({
    required this.key,
    required this.person,
    required this.startDate,
    required this.endDate,
  });

  factory Log.fromJson(Map<String, dynamic> json) => _$LogFromJson(json);
  Map<String, dynamic> toJson() => _$LogToJson(this);

  @override
  List<Object?> get props => [key, person, startDate, endDate];
}
