import 'package:flutter/material.dart';

class PageElement {
  final String name;
  final Widget page;
  final Icon icon;

  const PageElement({
    required this.name,
    required this.page,
    required this.icon,
  }); 
}
