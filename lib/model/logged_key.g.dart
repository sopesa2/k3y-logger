// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'logged_key.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

LoggedKey _$LoggedKeyFromJson(Map<String, dynamic> json) => LoggedKey(
      name: json['name'] as String,
      location: json['location'] as String,
      logTime: DateTime.parse(json['logTime'] as String),
      authenticationLevel:
          $enumDecode(_$AuthLevelEnumMap, json['authenticationLevel']),
      type: $enumDecodeNullable(_$KeyTypeEnumMap, json['type']),
      group: json['group'] as String?,
    );

Map<String, dynamic> _$LoggedKeyToJson(LoggedKey instance) => <String, dynamic>{
      'name': instance.name,
      'location': instance.location,
      'authenticationLevel': _$AuthLevelEnumMap[instance.authenticationLevel]!,
      'logTime': instance.logTime.toIso8601String(),
      'type': _$KeyTypeEnumMap[instance.type],
      'group': instance.group,
    };

const _$AuthLevelEnumMap = {
  AuthLevel.owner: 'owner',
  AuthLevel.security: 'security',
  AuthLevel.staff: 'staff',
  AuthLevel.anyone: 'anyone',
};

const _$KeyTypeEnumMap = {
  KeyType.key: 'key',
  KeyType.magnetCard: 'magnetCard',
  KeyType.nfcTag: 'nfcTag',
};
