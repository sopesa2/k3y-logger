import 'package:equatable/equatable.dart';
import 'package:k3y_logger/model/log.dart';

enum LogsStatePhase { UNINITIALIZED, LOADING, INITIALIZED }

enum LogFilterType { PERSON, KEY, DATE }

class LogsState extends Equatable {
  final LogsStatePhase phase;
  final dynamic? keyword;
  final List<Log>? allLogs;
  final List<Log>? filteredLogs;
  final LogFilterType? filterType;

  const LogsState({required this.phase, this.keyword, this.allLogs, this.filteredLogs, this.filterType});

  @override
  List<Object?> get props => [phase, keyword, allLogs, filteredLogs, filterType];
}
