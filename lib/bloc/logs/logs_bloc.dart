import 'dart:async';
import 'dart:convert';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:k3y_logger/bloc/logs/logs_event.dart';
import 'package:k3y_logger/bloc/logs/logs_state.dart';
import 'package:k3y_logger/core/is_same_date.dart';

import '../../model/log.dart';
import '../../services/local_storage_service.dart';

const STORED_LOGS = "stored_logs";

class LogsBloc extends Bloc<LogsEvent, LogsState> {
  final LocalStorageService _localStorageService = LocalStorageService();

  LogsBloc() : super(LogsState(phase: LogsStatePhase.UNINITIALIZED)) {
    on<LogsEventInitializeLogs>(_initializeLogsToState);
    on<LogsEventAddNewLog>(_addNewLogToState);
    on<LogsEventChangeFilter>(_changeFilterToState);
    on<LogsEventFilterLogs>(_filterLogsToState);
    on<LogsEventRemoveLog>(_removeLogToState);
  }

  FutureOr<void> _initializeLogsToState(LogsEventInitializeLogs event, Emitter<LogsState> emit) async {
    emit(LogsState(
      phase: LogsStatePhase.LOADING,
      keyword: state.keyword,
    ));

    List<dynamic> rawLogs = jsonDecode((await _localStorageService.getItem(STORED_LOGS) ?? "[]"));
    final List<Log> storedLogs = rawLogs.map((rawStoredLog) => Log.fromJson(rawStoredLog)).toList();
    final List<Log> filteredLogs = List.from(storedLogs);

    emit(LogsState(
      phase: LogsStatePhase.INITIALIZED,
      allLogs: storedLogs,
      filteredLogs: filteredLogs,
      keyword: state.keyword,
      filterType: state.filterType,
    ));
  }

  FutureOr<void> _addNewLogToState(LogsEventAddNewLog event, Emitter<LogsState> emit) async {
    emit(LogsState(
      phase: LogsStatePhase.LOADING,
      allLogs: state.allLogs,
      filteredLogs: state.filteredLogs,
      keyword: state.keyword,
      filterType: state.filterType,
    ));

    final List<Log> updatedStoredLogs = (state.allLogs ?? [])..add(event.newLog);
    final List<Log> updatedfilteredLogs = List.from(updatedStoredLogs);

    await _localStorageService.addNewItem(
      STORED_LOGS,
      jsonEncode(
        updatedStoredLogs.map((log) => log.toJson()).toList(),
      ),
    );

    emit(LogsState(
      phase: LogsStatePhase.INITIALIZED,
      allLogs: updatedStoredLogs,
      filteredLogs: updatedfilteredLogs,
      keyword: state.keyword,
      filterType: state.filterType,
    ));
  }

  FutureOr<void> _changeFilterToState(LogsEventChangeFilter event, Emitter<LogsState> emit) async {
    emit(LogsState(
      phase: LogsStatePhase.INITIALIZED,
      allLogs: state.allLogs,
      filteredLogs: state.filteredLogs,
      keyword: null,
      filterType: event.filterBy,
    ));
  }

  FutureOr<void> _filterLogsToState(LogsEventFilterLogs event, Emitter<LogsState> emit) async {
    final List<Log>? filteredLogs = state.allLogs?.where((log) {
      switch (state.filterType) {
        case LogFilterType.PERSON:
          return log.person.name.contains(event.keyword);
        case LogFilterType.KEY:
          return log.key.name.contains(event.keyword);
        case LogFilterType.DATE:
          return log.startDate.isSameDate(event.keyword) || log.endDate.isSameDate(event.keyword);
        default:
          return false;
      }
    }).toList();

    emit(LogsState(
      phase: LogsStatePhase.INITIALIZED,
      allLogs: state.allLogs,
      filteredLogs: filteredLogs,
      keyword: event.keyword,
      filterType: state.filterType,
    ));
  }

  FutureOr<void> _removeLogToState(LogsEventRemoveLog event, Emitter<LogsState> emit) async {
    emit(LogsState(
      phase: LogsStatePhase.LOADING,
      allLogs: state.allLogs,
      filteredLogs: state.filteredLogs,
      keyword: state.keyword,
      filterType: state.filterType,
    ));

    final List<Log> updatedStoredLogs = (state.allLogs ?? [])..removeWhere((log) => log == event.selectedLog);
    final List<Log> updatedfilteredLogs = List.from(updatedStoredLogs);

    await _localStorageService.addNewItem(
      STORED_LOGS,
      jsonEncode(
        updatedStoredLogs.map((log) => log.toJson()).toList(),
      ),
    );

    emit(LogsState(
      phase: LogsStatePhase.INITIALIZED,
      allLogs: updatedStoredLogs,
      filteredLogs: updatedfilteredLogs,
      keyword: state.keyword,
      filterType: state.filterType,
    ));
  }
}
