import 'package:equatable/equatable.dart';
import 'package:k3y_logger/bloc/logs/logs_state.dart';
import 'package:k3y_logger/model/log.dart';



abstract class LogsEvent extends Equatable {
  @override
  List<Object?> get props => [];
}

class LogsEventInitializeLogs extends LogsEvent {}

class LogsEventAddNewLog extends LogsEvent {
  final Log newLog;

  LogsEventAddNewLog({required this.newLog});

  @override
  List<Object?> get props => [newLog];
}

class LogsEventFilterLogs extends LogsEvent {
  final dynamic keyword;

  LogsEventFilterLogs({required this.keyword});

  @override
  List<Object?> get props => [keyword];
}

class LogsEventRemoveLog extends LogsEvent {
  final Log selectedLog;

  LogsEventRemoveLog({required this.selectedLog});

  @override
  List<Object?> get props => [selectedLog];
}

class LogsEventChangeFilter extends LogsEvent {
  final LogFilterType filterBy;

  LogsEventChangeFilter(this.filterBy);

  @override
  List<Object?> get props => [filterBy];
}
