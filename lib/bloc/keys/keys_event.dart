import 'package:equatable/equatable.dart';
import 'package:k3y_logger/model/logged_key.dart';

abstract class KeysEvent extends Equatable {
  @override
  List<Object?> get props => [];
}

class KeysEventInitializeKeys extends KeysEvent {}

class KeysEventAddNewKey extends KeysEvent {
  final LoggedKey newKey;

  KeysEventAddNewKey({required this.newKey});

  @override
  List<Object?> get props => [newKey];
}

class KeysEventFilterKeys extends KeysEvent {
  final String keyword;

  KeysEventFilterKeys({required this.keyword});

  @override
  List<Object?> get props => [keyword];
}
