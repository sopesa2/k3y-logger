import 'package:equatable/equatable.dart';
import 'package:k3y_logger/model/logged_key.dart';

enum KeysStatePhase { UNINITIALIZED, LOADING, INITIALIZED }

class KeysState extends Equatable {
  final KeysStatePhase phase;
  final String? keyword;
  final List<LoggedKey>? allKeys;
  final List<LoggedKey>? filteredKeys;

  const KeysState({
    required this.phase,
    this.keyword,
    this.allKeys,
    this.filteredKeys,
  });

  @override
  List<Object?> get props => [phase, allKeys, filteredKeys];
}
