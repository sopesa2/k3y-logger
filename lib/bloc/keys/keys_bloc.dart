import 'dart:async';
import 'dart:convert';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:k3y_logger/bloc/keys/keys_event.dart';
import 'package:k3y_logger/bloc/keys/keys_state.dart';
import 'package:k3y_logger/model/logged_key.dart';
import 'package:k3y_logger/services/local_storage_service.dart';

const String STORED_KEYS = "STORED_KEYS";

class KeysBloc extends Bloc<KeysEvent, KeysState> {
  final LocalStorageService _localStorageService = LocalStorageService();

  KeysBloc() : super(const KeysState(phase: KeysStatePhase.UNINITIALIZED)) {
    on<KeysEventInitializeKeys>(_initializeKeysToState);
    on<KeysEventAddNewKey>(_addKeyToState);
    on<KeysEventFilterKeys>(_filterKeysToState);
  }

  void _initializeKeysToState(KeysEventInitializeKeys event, Emitter<KeysState> emit) async {
    emit(KeysState(
      phase: KeysStatePhase.LOADING,
      keyword: state.keyword,
    ));

    List<dynamic> rawKeys = jsonDecode((await _localStorageService.getItem(STORED_KEYS) ?? "[]"));
    final List<LoggedKey> storedKeys = rawKeys.map((rawStoredKey) => LoggedKey.fromJson(rawStoredKey)).toList();
    final List<LoggedKey> filteredKeys = List.from(storedKeys);

    emit(KeysState(
      phase: KeysStatePhase.INITIALIZED,
      allKeys: storedKeys,
      filteredKeys: filteredKeys,
      keyword: state.keyword,
    ));
  }

  FutureOr<void> _addKeyToState(KeysEventAddNewKey event, Emitter<KeysState> emit) async {
    emit(KeysState(
      phase: KeysStatePhase.LOADING,
      allKeys: state.allKeys,
      filteredKeys: state.filteredKeys,
      keyword: state.keyword,
    ));

    List<LoggedKey> updatedStoredKeys = List.from(state.allKeys ?? [])..add(event.newKey);
    List<LoggedKey> updatedFilteredKeys = List.from(updatedStoredKeys);

    await _localStorageService.addNewItem(
      STORED_KEYS,
      jsonEncode(
        updatedStoredKeys.map((key) => key.toJson()).toList(),
      ),
    );

    emit(KeysState(
      phase: KeysStatePhase.INITIALIZED,
      allKeys: updatedStoredKeys,
      filteredKeys: updatedFilteredKeys,
      keyword: state.keyword,
    ));
  }

  FutureOr<void> _filterKeysToState(KeysEventFilterKeys event, Emitter<KeysState> emit) async {
    final List<LoggedKey>? filteredKeys = state.allKeys?.where((key) => key.name.contains(event.keyword)).toList();

    emit(KeysState(
      phase: KeysStatePhase.INITIALIZED,
      allKeys: state.allKeys,
      filteredKeys: filteredKeys,
      keyword: event.keyword,
    ));
  }
}
