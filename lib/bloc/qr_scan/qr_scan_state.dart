import 'package:equatable/equatable.dart';
import 'package:k3y_logger/model/logged_key.dart';

enum QrScanPhase { PROCESSING, READY, DONE, ERROR, NOT_K3Y }

class QrScanState extends Equatable {
  final QrScanPhase phase;
  final LoggedKey? scanResult;

  QrScanState({required this.phase, this.scanResult});

  @override
  List<Object?> get props => [phase, scanResult];
}
