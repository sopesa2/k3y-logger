import 'package:equatable/equatable.dart';

abstract class QrScanEvent extends Equatable {
  @override
  List<Object?> get props => [];
}

class QrScanEventScan extends QrScanEvent {
  final String? scanResult;

  QrScanEventScan(this.scanResult);

  @override
  List<Object?> get props => [scanResult];
}
