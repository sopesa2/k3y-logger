import 'dart:async';
import 'dart:convert';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:k3y_logger/bloc/qr_scan/qr_scan_event.dart';
import 'package:k3y_logger/bloc/qr_scan/qr_scan_state.dart';
import 'package:k3y_logger/model/logged_key.dart';

class QrScanBloc extends Bloc<QrScanEvent, QrScanState> {
  QrScanBloc() : super(QrScanState(phase: QrScanPhase.READY)) {
    LoggedKey testkey = LoggedKey(
      name: 'asd',
      location: 'location',
      logTime: DateTime.now(),
      authenticationLevel: AuthLevel.anyone,
    );
    print(jsonEncode(testkey.toJson()));
    on<QrScanEventScan>(_eventScanToState);
  }

  FutureOr<void> _eventScanToState(QrScanEventScan event, Emitter<QrScanState> emit) async {
    emit(QrScanState(phase: QrScanPhase.PROCESSING));
    if (event.scanResult == null) {
      emit(QrScanState(phase: QrScanPhase.ERROR));
      emit(QrScanState(phase: QrScanPhase.READY));
      return;
    }
    if (!event.scanResult!.startsWith('k3ylog:')) {
      emit(QrScanState(phase: QrScanPhase.NOT_K3Y));
      emit(QrScanState(phase: QrScanPhase.READY));
      return;
    }
    try {
      String rawLoggedKey = event.scanResult!.substring(7);
      print(rawLoggedKey);
      emit(
        QrScanState(
          phase: QrScanPhase.DONE,
          scanResult: LoggedKey.fromJson(
            jsonDecode(rawLoggedKey),
          ),
        ),
      );
    } catch (e) {
      emit(QrScanState(phase: QrScanPhase.ERROR));
      emit(QrScanState(phase: QrScanPhase.READY));
      return;
    }
  }
}
