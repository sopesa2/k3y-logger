import 'package:equatable/equatable.dart';
import 'package:k3y_logger/model/person.dart';

enum PeopleStatePhase { UNINITIALIZED, LOADING, INITIALIZED }

class PeopleState extends Equatable {
  final PeopleStatePhase phase;
  final String? keyword;
  final List<Person>? allPeople;
  final List<Person>? filteredPeople;

  PeopleState({
    required this.phase,
    this.keyword,
    this.allPeople,
    this.filteredPeople,
  });

  @override
  List<Object?> get props => [phase, allPeople, filteredPeople];
}
