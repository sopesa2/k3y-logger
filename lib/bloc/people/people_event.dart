import 'package:equatable/equatable.dart';
import 'package:k3y_logger/model/person.dart';

abstract class PeopleEvent extends Equatable {
  @override
  List<Object?> get props => [];
}

class PeopleEventInitializePeople extends PeopleEvent {}

class PeopleEventAddNewPerson extends PeopleEvent {
  final Person newPerson;

  PeopleEventAddNewPerson({required this.newPerson});

  @override
  List<Object?> get props => [newPerson];
}

class PeopleEventFilterPeople extends PeopleEvent {
  final String keyword;

  PeopleEventFilterPeople({required this.keyword});

  @override
  List<Object?> get props => [keyword];
}

class PeopleEventRemovePerson extends PeopleEvent {
  final Person selectedPerson;

  PeopleEventRemovePerson({required this.selectedPerson});

  @override
  List<Object?> get props => [selectedPerson];
}
