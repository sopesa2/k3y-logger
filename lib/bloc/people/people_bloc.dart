import 'dart:async';
import 'dart:convert';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:k3y_logger/bloc/people/people_event.dart';
import 'package:k3y_logger/bloc/people/people_state.dart';
import 'package:k3y_logger/model/person.dart';
import 'package:k3y_logger/services/local_storage_service.dart';

const STORED_PEOPLE = "stored_people";

class PeopleBloc extends Bloc<PeopleEvent, PeopleState> {
  final LocalStorageService _localStorageService = LocalStorageService();

  PeopleBloc() : super(PeopleState(phase: PeopleStatePhase.UNINITIALIZED)) {
    on<PeopleEventInitializePeople>(_initializePeopleToState);
    on<PeopleEventAddNewPerson>(_addNewPersonToState);
    on<PeopleEventFilterPeople>(_filterPeopleToState);
    on<PeopleEventRemovePerson>(_removePersonToState);
  }

  FutureOr<void> _initializePeopleToState(PeopleEventInitializePeople event, Emitter<PeopleState> emit) async {
    emit(PeopleState(
      phase: PeopleStatePhase.LOADING,
      keyword: state.keyword,
    ));

    List<dynamic> rawPeople = jsonDecode((await _localStorageService.getItem(STORED_PEOPLE) ?? "[]"));
    final List<Person> storedPeople = rawPeople.map((rawStoredPerson) => Person.fromJson(rawStoredPerson)).toList();
    final List<Person> filteredPeople = List.from(storedPeople);

    emit(PeopleState(
      phase: PeopleStatePhase.INITIALIZED,
      allPeople: storedPeople,
      filteredPeople: filteredPeople,
      keyword: state.keyword,
    ));
  }

  FutureOr<void> _addNewPersonToState(PeopleEventAddNewPerson event, Emitter<PeopleState> emit) async {
    emit(PeopleState(
      phase: PeopleStatePhase.LOADING,
      allPeople: state.allPeople,
      filteredPeople: state.filteredPeople,
      keyword: state.keyword,
    ));

    final List<Person> updatedStoredPeople = (state.allPeople ?? [])..add(event.newPerson);
    final List<Person> updatedFilteredPeople = List.from(updatedStoredPeople);

    await _localStorageService.addNewItem(
      STORED_PEOPLE,
      jsonEncode(
        updatedStoredPeople.map((person) => person.toJson()).toList(),
      ),
    );

    emit(PeopleState(
      phase: PeopleStatePhase.INITIALIZED,
      allPeople: updatedStoredPeople,
      filteredPeople: updatedFilteredPeople,
      keyword: state.keyword,
    ));
  }

  FutureOr<void> _filterPeopleToState(PeopleEventFilterPeople event, Emitter<PeopleState> emit) async {
    final List<Person>? filteredPeople = state.allPeople?.where((key) => key.name.contains(event.keyword)).toList();

    emit(PeopleState(
      phase: PeopleStatePhase.INITIALIZED,
      allPeople: state.allPeople,
      filteredPeople: filteredPeople,
      keyword: event.keyword,
    ));
  }

  FutureOr<void> _removePersonToState(PeopleEventRemovePerson event, Emitter<PeopleState> emit) async {
    emit(PeopleState(
      phase: PeopleStatePhase.LOADING,
      allPeople: state.allPeople,
      filteredPeople: state.filteredPeople,
      keyword: state.keyword,
    ));

    final List<Person> updatedStoredPeople = (state.allPeople ?? [])..removeWhere((person) => person == event.selectedPerson);
    final List<Person> updatedFilteredPeople = List.from(updatedStoredPeople);

    await _localStorageService.addNewItem(
      STORED_PEOPLE,
      jsonEncode(
        updatedStoredPeople.map((person) => person.toJson()).toList(),
      ),
    );

    emit(PeopleState(
      phase: PeopleStatePhase.INITIALIZED,
      allPeople: updatedStoredPeople,
      filteredPeople: updatedFilteredPeople,
      keyword: state.keyword,
    ));
  }
}
