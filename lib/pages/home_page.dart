//import 'dart:js';

import 'package:flutter/material.dart';
import 'package:k3y_logger/model/page_element.dart';
import 'package:k3y_logger/pages/logs_page.dart';
import 'package:k3y_logger/pages/people_page.dart';
import 'package:k3y_logger/pages/settings_page.dart';
import 'package:k3y_logger/pages/keys_page.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() {
    return _HomePageState();
  }
}

class _HomePageState extends State<HomePage> {
  int _selectedIndex = 0;

  static final List<PageElement> _pageOptions = [
    PageElement(
      name: 'Keys',
      page: KeysPage(),
      icon: const Icon(Icons.key),
    ),
    PageElement(
      name: 'Logs',
      page: LogsPage(),
      icon: const Icon(Icons.search),
    ),
    const PageElement(
      name: 'People',
      page: PeoplePage(),
      icon: Icon(Icons.people),
    ),
    PageElement(
      name: 'Settings',
      page: SettingsPage(),
      icon: Icon(Icons.settings),
    ),
  ];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      appBar: AppBar(
        title: Text(
          _pageOptions.elementAt(_selectedIndex).name,
          style: TextStyle(
            color: Colors.teal.shade100,
          ),
        ),
      ),
      body: _pageOptions.elementAt(_selectedIndex).page,
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _selectedIndex,
        selectedItemColor: Colors.teal.shade100,
        unselectedItemColor: Colors.teal.shade800,
        showSelectedLabels: false,
        onTap: _onItemTapped,
        items: _pageOptions
            .map<BottomNavigationBarItem>(
              (pageOption) => BottomNavigationBarItem(
                icon: pageOption.icon,
                label: pageOption.name,
                backgroundColor: Colors.teal,
              ),
            )
            .toList(),
      ),
    );
  }

  static void function() {
    print('asd');
  }
}
