import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:k3y_logger/bloc/people/people_bloc.dart';
import 'package:k3y_logger/bloc/people/people_event.dart';
import 'package:k3y_logger/bloc/people/people_state.dart';
import 'package:k3y_logger/core/date_format.dart';
import 'package:k3y_logger/widgets/create_person_dialog.dart';
import 'package:k3y_logger/widgets/custom_add_button.dart';
import 'package:k3y_logger/widgets/custom_data_row.dart';
import 'package:k3y_logger/widgets/custom_search_field.dart';
import 'package:k3y_logger/widgets/custom_expandable_content.dart';

import '../model/person.dart';

class PeoplePage extends StatelessWidget {
  const PeoplePage({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<PeopleBloc, PeopleState>(
      builder: (context, peopleState) {
        return Scaffold(
          resizeToAvoidBottomInset: true,
          body: Stack(
            children: [
              if (peopleState.phase == PeopleStatePhase.LOADING)
                const Center(
                  child: CircularProgressIndicator(),
                ),
              Column(
                children: [
                  Row(
                    children: [
                      Expanded(
                        flex: 6,
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(2, 4, 1, 3),
                          child: CustomSearchField(
                            searchFunction: (keyword) =>
                                BlocProvider.of<PeopleBloc>(context).add(
                                    PeopleEventFilterPeople(keyword: keyword)),
                            initialValue: peopleState.keyword,
                          ),
                        ),
                      ),
                      Expanded(
                        flex: 1,
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(0, 2, 3, 2),
                          child: CustomAddButton(
                            (() => showDialog<String>(
                                  context: context,
                                  builder: (BuildContext context) =>
                                      CreatePersonDialog(),
                                )),
                          ),
                        ),
                      )
                    ],
                  ),
                  Expanded(
                    flex: 10,
                    child: ListView.builder(
                        itemCount: peopleState.filteredPeople!.length,
                        itemBuilder: (context, index) {
                          return CustomExpandableContent(
                            title: peopleState.filteredPeople![index].name,
                            content: Container(
                              alignment: Alignment.centerLeft,
                              padding: const EdgeInsets.fromLTRB(20, 5, 20, 5),
                              child: Column(
                                children: [
                                  CustomDataRow(
                                    title: "Email:",
                                    data: peopleState
                                        .filteredPeople![index].email,
                                  ),
                                  CustomDataRow(
                                    title: "Phone:",
                                    data: peopleState
                                        .filteredPeople![index].phoneNumber,
                                  ),
                                  CustomDataRow(
                                    title: "Date of birth:",
                                    data: getFormattedDateLong(peopleState
                                        .filteredPeople![index].dateOfBirth),
                                  ),
                                ],
                              ),
                            ),
                          );
                        }),
                  ),
                ],
              ),
            ],
          ),
        );
      },
    );
  }
}
