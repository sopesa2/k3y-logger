import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:k3y_logger/bloc/logs/logs_bloc.dart';
import 'package:k3y_logger/bloc/logs/logs_event.dart';
import 'package:k3y_logger/bloc/logs/logs_state.dart';
import 'package:k3y_logger/core/date_format.dart';
import 'package:k3y_logger/model/log.dart';
import 'package:k3y_logger/model/logged_key.dart';
import 'package:k3y_logger/model/person.dart';
import 'package:k3y_logger/widgets/create_log_dialog.dart';
import 'package:k3y_logger/widgets/custom_add_button.dart';
import 'package:k3y_logger/widgets/custom_search_field.dart';
import 'package:k3y_logger/widgets/custom_expandable_content.dart';
import 'package:k3y_logger/widgets/custom_tab_item.dart';

class LogsPage extends StatelessWidget {
  const LogsPage({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<LogsBloc, LogsState>(
      builder: (context, logsState) {
        return Scaffold(
          resizeToAvoidBottomInset: true,
          body: Column(
            children: [
              Row(
                children: [
                  Expanded(
                    flex: 6,
                    child: (logsState.filterType != LogFilterType.DATE)
                        ? Padding(
                            padding: const EdgeInsets.fromLTRB(2, 5, 1, 2),
                            child: CustomSearchField(
                              searchFunction: (keyword) => BlocProvider.of<LogsBloc>(context).add(LogsEventFilterLogs(keyword: keyword)),
                              initialValue: logsState.keyword,
                            ),
                          )
                        : Row(
                            children: [
                              IconButton(
                                iconSize: 35,
                                onPressed: () async => BlocProvider.of<LogsBloc>(context).add(
                                  LogsEventFilterLogs(
                                    keyword: await showDatePicker(
                                      context: context,
                                      initialDate: DateTime.now(),
                                      firstDate: DateTime(2020),
                                      lastDate: DateTime(2100),
                                    ),
                                  ),
                                ),
                                icon: Icon(
                                  Icons.calendar_month,
                                  color: Colors.teal.shade800,
                                ),
                              ),
                              (logsState.keyword != null)
                                  ? Text(
                                      getFormattedDateLong(logsState.keyword as DateTime),
                                      style: TextStyle(color: Colors.teal.shade500, fontSize: 20, fontWeight: FontWeight.w500),
                                    )
                                  : Text(
                                      "Select date to filter by!",
                                      style: TextStyle(color: Colors.teal.shade200, fontSize: 20, fontWeight: FontWeight.w500),
                                    ),
                            ],
                          ),
                  ),
                  Expanded(
                    flex: 1,
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(0, 15, 3, 14),
                      child: CustomAddButton(
                        (() => showDialog<String>(
                              context: context,
                              builder: (BuildContext context) => CreateLogDialog(),

                              // BlocProvider.of<LogsBloc>(context).add(
                              //       LogsEventAddNewLog(
                              //         newLog: Log(
                              //           key: LoggedKey(
                              //             name: 'key name',
                              //             location: 'location',
                              //             logTime: DateTime.now(),
                              //             authenticationLevel: AuthLevel.anyone,
                              //           ),
                              //           person: Person(
                              //             name: "person name",
                              //             email: "email",
                              //             phoneNumber: "${DateTime.now().microsecondsSinceEpoch}",
                              //             dateOfBirth: DateTime.now(),
                              //           ),
                              //           startDate: DateTime(2023, 1, 5),
                              //           endDate: DateTime(2023, 4, 5),
                              //         ),
                              //       ),
                              //     )),
                            )),
                      ),
                    ),
                  ),
                ],
              ),
              Row(
                children: [
                  CustomTabItem(
                    icon: Icons.account_circle,
                    onTapFunction: () => BlocProvider.of<LogsBloc>(context).add(LogsEventChangeFilter(LogFilterType.PERSON)),
                    active: logsState.filterType == LogFilterType.PERSON,
                  ),
                  CustomTabItem(
                    icon: Icons.key,
                    onTapFunction: () => BlocProvider.of<LogsBloc>(context).add(LogsEventChangeFilter(LogFilterType.KEY)),
                    active: logsState.filterType == LogFilterType.KEY,
                  ),
                  CustomTabItem(
                    icon: Icons.date_range,
                    onTapFunction: () => BlocProvider.of<LogsBloc>(context).add(LogsEventChangeFilter(LogFilterType.DATE)),
                    active: logsState.filterType == LogFilterType.DATE,
                  ),
                ],
              ),
              Expanded(
                flex: 10,
                child: ListView.builder(
                    itemCount: logsState.filteredLogs!.length,
                    itemBuilder: (context, index) {
                      return CustomExpandableContent(
                        title: logsState.filteredLogs![index].person.name,
                        content: Padding(
                          padding: EdgeInsets.symmetric(horizontal: 10),
                          child: Row(
                            children: [
                              Expanded(
                                flex: 8,
                                child: Text(
                                  logsState.filteredLogs![index].key.name,
                                  style: TextStyle(fontSize: 15, fontWeight: FontWeight.w500),
                                ),
                              ),
                              Expanded(
                                flex: 15,
                                child: Text(
                                  "${getFormattedDateLong(logsState.filteredLogs![index].startDate)} - ${getFormattedDateLong(logsState.filteredLogs![index].endDate)}.",
                                  style: const TextStyle(fontSize: 15, fontWeight: FontWeight.w300),
                                  overflow: TextOverflow.visible,
                                ),
                              ),
                            ],
                          ),
                        ),
                      );
                    }),
              ),
            ],
          ),
        );
      },
    );
  }
}
