import 'package:flutter/material.dart';
import 'package:k3y_logger/widgets/custom_button.dart';
import 'package:k3y_logger/widgets/custom_text_field.dart';

class SettingsPage extends StatelessWidget {
  const SettingsPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      body: Column(
        children: [
          Container(
            alignment: Alignment.center,
            padding: const EdgeInsets.fromLTRB(4, 20, 4, 10),
            child: CustomTextField(
              postfix: Icon(Icons.person),
              hintText: "Set your name...",
            ),
          ),
          Container(
            alignment: Alignment.center,
            padding: const EdgeInsets.fromLTRB(4, 10, 4, 0),
            child: CustomTextField(
              postfix: Icon(Icons.alternate_email),
              hintText: "Set your email...",
            ),
          ),
          Expanded(
            child: ListView(
              reverse: true,
              children: [
                Container(
                  alignment: Alignment.center,
                  padding: const EdgeInsets.fromLTRB(40, 10, 40, 10),
                  child: CustomButton(
                    action: function,
                    label: 'Delete all keys',
                    importanceStyle: Importance.Warning,
                  ),
                ),
                Container(
                  alignment: Alignment.center,
                  padding: const EdgeInsets.fromLTRB(40, 10, 40, 10),
                  child: CustomButton(
                    action: function,
                    label: 'Delete all people',
                    importanceStyle: Importance.Warning,
                  ),
                ),
                Container(
                  alignment: Alignment.center,
                  padding: const EdgeInsets.fromLTRB(40, 10, 40, 10),
                  child: CustomButton(
                    action: function,
                    label: 'Delete all logs',
                    importanceStyle: Importance.Warning,
                  ),
                ),
                Container(
                  alignment: Alignment.center,
                  padding: const EdgeInsets.fromLTRB(40, 10, 40, 10),
                  child: CustomButton(
                    action: function,
                    label: 'Save settings',
                    importanceStyle: Importance.Primary,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  void function() {
    print("Nobody expects the spanish inquisition");
  }
}
