import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:k3y_logger/bloc/keys/keys_bloc.dart';
import 'package:k3y_logger/bloc/keys/keys_event.dart';
import 'package:k3y_logger/bloc/keys/keys_state.dart';
import 'package:k3y_logger/core/date_format.dart';
import 'package:k3y_logger/model/logged_key.dart';
import 'package:k3y_logger/pages/qr_scan_page.dart';
import 'package:k3y_logger/widgets/custom_add_button.dart';
import 'package:k3y_logger/widgets/custom_data_row.dart';
import 'package:k3y_logger/widgets/custom_search_field.dart';
import 'package:k3y_logger/widgets/custom_expandable_content.dart';
import 'package:k3y_logger/widgets/generate_qr_code_button.dart';

class KeysPage extends StatelessWidget {
  const KeysPage({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<KeysBloc, KeysState>(
      builder: (context, keysState) {
        return Scaffold(
          floatingActionButton: FloatingActionButton(
            onPressed: (() {
              Navigator.push(context, MaterialPageRoute(builder: (context) => QrScanPage()));
            }),
            child: const Icon(
              Icons.qr_code_2,
              size: 30,
            ),
          ),
          resizeToAvoidBottomInset: true,
          body: Stack(
            children: [
              if (keysState.phase == KeysStatePhase.LOADING)
                const Center(
                  child: CircularProgressIndicator(),
                ),
              Column(
                children: [
                  Row(
                    children: [
                      Expanded(
                        flex: 6,
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(2, 4, 1, 3),
                          child: CustomSearchField(
                            searchFunction: (keyword) => BlocProvider.of<KeysBloc>(context).add(KeysEventFilterKeys(keyword: keyword)),
                            initialValue: keysState.keyword,
                          ),
                        ),
                      ),
                      Expanded(
                        flex: 1,
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(0, 2, 3, 2),
                          child: CustomAddButton(
                            (() => BlocProvider.of<KeysBloc>(context).add(
                                  KeysEventAddNewKey(
                                    newKey: LoggedKey(
                                      name: 'newTestKey-${DateTime.now().millisecondsSinceEpoch}',
                                      location: 'newTestKeyLocation',
                                      type: KeyType.key,
                                      group: 'Roomkeys',
                                      authenticationLevel: AuthLevel.anyone,
                                      logTime: DateTime.now(),
                                    ),
                                  ),
                                )),
                          ),
                        ),
                      )
                    ],
                  ),
                  Expanded(
                    flex: 10,
                    child: ListView.builder(
                        itemCount: keysState.filteredKeys?.length ?? 0,
                        itemBuilder: (context, index) {
                          return CustomExpandableContent(
                            title: keysState.filteredKeys![index].name,
                            content: Container(
                              alignment: Alignment.centerLeft,
                              padding: const EdgeInsets.fromLTRB(20, 5, 20, 0),
                              child: Column(
                                children: [
                                  CustomDataRow(title: "Location:", data: keysState.filteredKeys![index].location),
                                  CustomDataRow(title: "Security level:", data: keysState.filteredKeys![index].authenticationLevel.authLevelName),
                                  CustomDataRow(title: "Type:", data: keysState.filteredKeys![index].type?.typeName),
                                  CustomDataRow(title: "Date:", data: getFormattedDateLong(keysState.filteredKeys![index].logTime)),
                                  SizedBox(height: 10),
                                  GenerateQrCodeButton(loggedKey: keysState.filteredKeys![index]),
                                ],
                              ),
                            ),
                          );
                        }),
                  ),
                ],
              ),
            ],
          ),
        );
      },
    );
  }
}
