import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:k3y_logger/bloc/qr_scan/qr_scan_bloc.dart';
import 'package:k3y_logger/bloc/qr_scan/qr_scan_event.dart';
import 'package:k3y_logger/bloc/qr_scan/qr_scan_state.dart';
import 'package:k3y_logger/core/show_snackbar.dart';
import 'package:k3y_logger/pages/key_info_page.dart';
import 'package:mobile_scanner/mobile_scanner.dart';

class QrScanPage extends StatelessWidget {
  const QrScanPage({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      lazy: false,
      create: (BuildContext context) => QrScanBloc(),
      child: BlocConsumer<QrScanBloc, QrScanState>(
        listener: (context, qrState) {
          WidgetsBinding.instance.addPostFrameCallback((_) {
            if (qrState.phase == QrScanPhase.DONE) {
              showSnackbar(
                message: "K3y code recognised!",
                color: Colors.green,
                context: context,
              );
              _onValidScan(context, qrState);
            } else if (qrState.phase == QrScanPhase.NOT_K3Y) {
              showSnackbar(
                message: "Scanned code is not a K3y code!",
                color: Colors.red,
                context: context,
              );
            } else if (qrState.phase == QrScanPhase.ERROR) {
              showSnackbar(
                message: "Error processing K3y code!",
                color: Colors.red,
                context: context,
              );
            }
          });
        },
        builder: (context, qrState) {
          return Scaffold(
            appBar: AppBar(title: const Text("Scan QR")),
            resizeToAvoidBottomInset: true,
            body: Stack(
              children: [
                MobileScanner(
                  onDetect: (barcode, args) {
                    if (barcode.rawValue == null) {
                      showSnackbar(
                        message: "Error scanning K3y code!",
                        color: Colors.red,
                        context: context,
                      );
                    } else {
                      BlocProvider.of<QrScanBloc>(context).add(QrScanEventScan(barcode.rawValue));
                    }
                  },
                ),
                Align(
                  alignment: Alignment.bottomCenter,
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(20, 0, 20, 50),
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(15),
                        color: Colors.teal.shade600,
                      ),
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Row(
                          children: [
                            const Padding(
                              padding: EdgeInsets.all(8.0),
                              child: Icon(
                                Icons.info,
                                color: Colors.white,
                                size: 30,
                              ),
                            ),
                            const Flexible(
                              child: Text(
                                "Scan key QR code to display information",
                                maxLines: 3,
                                overflow: TextOverflow.ellipsis,
                                style: TextStyle(
                                  fontSize: 20,
                                  color: Colors.white,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          );
        },
      ),
    );
  }

  void _onValidScan(BuildContext context, QrScanState qrState) {
    Navigator.pushReplacement(
      context,
      MaterialPageRoute(
        builder: (context) => KeyInfoPage(
          name: qrState.scanResult?.name,
          location: qrState.scanResult?.location,
          tempOwner: 'Kala Pál',
          keyType: qrState.scanResult?.type?.name,
          keyGroup: qrState.scanResult?.group,
          authLevel: qrState.scanResult?.authenticationLevel.name,
        ),
      ),
    );
  }
}
