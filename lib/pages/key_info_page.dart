import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

class KeyInfoPage extends StatelessWidget {
  const KeyInfoPage({
    required this.name,
    required this.location,
    required this.tempOwner,
    required this.keyType,
    required this.keyGroup,
    required this.authLevel,
    super.key,
  });

  final String? name;
  final String? tempOwner;
  final String? location;
  final String? keyType;
  final String? keyGroup;
  final String? authLevel;
  //final DateTime logTime;

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Text(
            "Key Info",
            style: TextStyle(
              color: Colors.teal.shade100,
            ),
          ),
        ),
        body: Stack(
          children: [
            Padding(
              padding: const EdgeInsets.fromLTRB(8, 50, 8, 0),
              child: Container(
                padding: const EdgeInsets.all(12),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(15),
                  color: Colors.teal.shade100,
                ),
                child: SizedBox(
                  height: 325.0,
                  child: ListView(
                    children: [
                      Icon(
                        Icons.info,
                        size: 50.0,
                        color: Colors.teal.shade400,
                      ),
                      Container(
                        child: Row(
                          children: [
                            Icon(
                              Icons.key,
                              size: 40.0,
                              color: Colors.teal.shade50,
                            ),
                            Expanded(
                              flex: 1,
                              child: SingleChildScrollView(
                                scrollDirection: Axis.horizontal,
                                child: Text(
                                  "Name: " + (name ?? ''),
                                  style: TextStyle(
                                      fontSize: 25, color: Colors.teal.shade50),
                                ),
                              ),
                            ),
                          ],
                        ),
                        decoration: BoxDecoration(
                          color: Colors.teal.shade400,
                          borderRadius: BorderRadius.circular(12),
                        ),
                      ),
                      Container(
                        child: Row(
                          children: [
                            Icon(
                              Icons.person,
                              size: 40.0,
                              color: Colors.teal.shade400,
                            ),
                            Expanded(
                              flex: 1,
                              child: SingleChildScrollView(
                                scrollDirection: Axis.horizontal,
                                child: Text(
                                  "Owner: " + (tempOwner ?? ''),
                                  style: TextStyle(
                                      fontSize: 25,
                                      color: Colors.teal.shade400),
                                ),
                              ),
                            ),
                          ],
                        ),
                        decoration: BoxDecoration(
                          color: Colors.teal.shade50,
                          borderRadius: BorderRadius.circular(12),
                        ),
                      ),
                      Container(
                        child: Row(
                          children: [
                            Icon(
                              Icons.location_pin,
                              size: 40.0,
                              color: Colors.teal.shade50,
                            ),
                            Expanded(
                              flex: 1,
                              child: SingleChildScrollView(
                                scrollDirection: Axis.horizontal,
                                child: Text(
                                  "Location: " + (location ?? ''),
                                  style: TextStyle(
                                      fontSize: 25, color: Colors.teal.shade50),
                                ),
                              ),
                            ),
                          ],
                        ),
                        decoration: BoxDecoration(
                          color: Colors.teal.shade400,
                          borderRadius: BorderRadius.circular(12),
                        ),
                      ),
                      Container(
                        child: Row(
                          children: [
                            Icon(
                              Icons.maps_home_work,
                              size: 40.0,
                              color: Colors.teal.shade400,
                            ),
                            Expanded(
                              flex: 1,
                              child: SingleChildScrollView(
                                scrollDirection: Axis.horizontal,
                                child: Text(
                                  "Key Type: " + (keyType ?? ''),
                                  style: TextStyle(
                                      fontSize: 25,
                                      color: Colors.teal.shade400),
                                ),
                              ),
                            ),
                          ],
                        ),
                        decoration: BoxDecoration(
                          color: Colors.teal.shade50,
                          borderRadius: BorderRadius.circular(12),
                        ),
                      ),
                      Container(
                        child: Row(
                          children: [
                            Icon(
                              Icons.apps,
                              size: 40.0,
                              color: Colors.teal.shade50,
                            ),
                            Expanded(
                              flex: 1,
                              child: SingleChildScrollView(
                                scrollDirection: Axis.horizontal,
                                child: Text(
                                  "Key Group: " + (keyGroup ?? ''),
                                  style: TextStyle(
                                      fontSize: 25, color: Colors.teal.shade50),
                                ),
                              ),
                            ),
                          ],
                        ),
                        decoration: BoxDecoration(
                          color: Colors.teal.shade400,
                          borderRadius: BorderRadius.circular(12),
                        ),
                      ),
                      Container(
                        child: Row(
                          children: [
                            Icon(
                              Icons.lock,
                              size: 40.0,
                              color: Colors.teal.shade400,
                            ),
                            Expanded(
                              flex: 1,
                              child: SingleChildScrollView(
                                scrollDirection: Axis.horizontal,
                                child: Text(
                                  "Auth level: " + (authLevel ?? ''),
                                  style: TextStyle(
                                      fontSize: 25,
                                      color: Colors.teal.shade400),
                                ),
                              ),
                            ),
                          ],
                        ),
                        decoration: BoxDecoration(
                          color: Colors.teal.shade50,
                          borderRadius: BorderRadius.circular(12),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
