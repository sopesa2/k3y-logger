import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:k3y_logger/bloc/keys/keys_bloc.dart';
import 'package:k3y_logger/bloc/keys/keys_event.dart';
import 'package:k3y_logger/bloc/logs/logs_bloc.dart';
import 'package:k3y_logger/bloc/logs/logs_event.dart';
import 'package:k3y_logger/bloc/people/people_bloc.dart';
import 'package:k3y_logger/bloc/people/people_event.dart';
import 'package:k3y_logger/pages/home_page.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<LogsBloc>(create: (BuildContext context) => LogsBloc()..add(LogsEventInitializeLogs()), lazy: false),
        BlocProvider<KeysBloc>(create: (BuildContext context) => KeysBloc()..add(KeysEventInitializeKeys()), lazy: false),
        BlocProvider<PeopleBloc>(create: (BuildContext context) => PeopleBloc()..add(PeopleEventInitializePeople()), lazy: false),
      ],
      child: MaterialApp(
        title: 'K3y Logger',
        theme: ThemeData(
          primarySwatch: Colors.teal,
        ),
        home: const SafeArea(
          child: HomePage(),
        ),
      ),
    );
  }
}
